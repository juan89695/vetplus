-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-07-2020 a las 06:01:46
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vetplus`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` mediumint(9) NOT NULL,
  `user` varchar(30) NOT NULL,
  `date` varchar(30) NOT NULL,
  `cliente` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`id`, `user`, `date`, `cliente`, `correo`) VALUES
(5, 'castro97', '2020-07-30 15:00:00', 'Pablo Paz', 'pablopaz@gmail.com'),
(6, 'castro97', '2020-07-30 15:00:00', 'Rodrigo Diaz', 'rodrigod@gmail.com'),
(7, 'castro97', '2020-07-30 15:00:00', 'Andres vergara', 'andresv@gmail.com'),
(8, 'castro97', '2020-07-30 15:00:00', 'Diego Marin', 'diegov@gmail.com'),
(9, 'castro97', '2020-07-30 15:00:00', 'Rocio Duarte', 'rociod@gmail.com'),
(10, 'castro97', '2020-07-30 15:00:00', 'Pablo Paz', 'pablopaz@gmail.com'),
(11, 'gaby97', '2020-07-30 15:00:00', 'Rodrigo alvarez', 'rodrigoa@gmail.com'),
(14, 'juan9878', '2020-08-30 20:00', 'alv', 'alv'),
(16, 'juan9878', '2020-08-30 20:00', 'alv', 'alv'),
(17, 'juan9878', '2020-08-30 20:00', 'alv', 'alv'),
(18, 'gaby97', '2020-07-30 15:00', 'Rodrigo alsvsdvarez', 'rodrigoa@gmail.com'),
(19, 'juan9878', 'sdcasd', 'sdac', 'sada'),
(20, 'juan9878', 'sdcasd', 'sdavdsvewq', 'sada'),
(21, 'juan9878', 'sdcasd', 'sdac', 'sad'),
(22, 'juan9878', 'sdcasd', 'sdac', 'sad'),
(23, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(24, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(25, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(26, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(27, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(28, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(29, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(30, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(31, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(32, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(33, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(34, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(35, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(36, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(37, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(38, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(39, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(40, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(41, 'juan9878', 'alv214123', 'sdac', 'sadsav3523'),
(42, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(43, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(44, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(45, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(46, 'juan9878', 'sdcasd', 'sdac', 'sadsav3523'),
(47, 'juan9878', 'sadc', 'sav', 'sad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `username` varchar(20) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `rol` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`username`, `fullname`, `mail`, `rol`, `password`) VALUES
('castro97', 'Juan Castro', 'prueba@gmail.com', 'veterinari', '$2y$10$kRRf5tJnZZC1ziom3pn9LOyEshX8XF7vXau14Jv9363a7rRZtEieq'),
('gaby97', 'Gabriela Gomez', 'gabybuhh@gmail.com', 'administra', '$2y$10$fWMtB8qieio.F5KA/Mwe.OCZUFwa68rbvKbFaYhueJitTuhPwSsfe'),
('juan9878', '', 'juan89695@gmail.com', 'admin', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Asignatura` (`user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `fk_Asignatura` FOREIGN KEY (`user`) REFERENCES `users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
