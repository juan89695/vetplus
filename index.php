<?php

include_once 'php/user.php';
include_once 'php/user_session.php';

$userSession = new UserSession();
$user = new User();
$result = "";

if(isset($_SESSION['user'])){

    $user->setUser($userSession->getCurrentUser());
    include_once 'php/main.php';

}else if(isset($_POST['username']) && isset($_POST['password'])){
    $userForm = $_POST['username'];
    $passForm = $_POST['password'];

    if($user->userExists($userForm, $passForm)){
        $userSession->setCurrentUser($userForm);
        $user->setUser($userForm);
        include_once 'php/main.php';
    }else{
        $errorLogin = "Nombre de usuario y/o contraseña es incorrecto";
        include_once 'php/login.php';
    }

}else{
    include_once 'php/login.php';
}

?>