<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles2.css">

    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="jtable/themes/lightcolor/blue/jtable.css">  

</head>
<body>
  <div class="d-flex" id="wrapper">
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <img src="img/logoMin.png " width="150px" height="60px" alt="" > </div>
      <div class="list-group list-group-flush">
        <a href="#" id="registrousers" class="list-group-item list-group-item-action bg-light">Agregar usuarios</a>
        <a href="#" id="vercitas" class="list-group-item list-group-item-action bg-light">Ver citas</a>
        <a href="php/logout.php" id="logout" class="list-group-item list-group-item-action bg-light red">Cerrar sesion</a>
      </div>

    </div>

    <div id="admin-content-wrapper">
        <h2>Bienvenido <?php echo $user->getNombre(); ?></h2>      
    </div>
  </div>
<script  src="https://code.jquery.com/jquery-3.5.1.min.js"  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="js/app.js"></script>

</body>

</html>

