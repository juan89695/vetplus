<?php 

echo('

<script  src="https://code.jquery.com/jquery-3.5.1.min.js"  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="jquery-ui/jquery-ui.js"></script>
<script src="jtable/jquery.jtable.js"></script>

<div class="tablapos" id="personas" style="width: 600px; display: absolute;
margin-left: 50px;
margin-top: 100px;"></div>  

<script type="text/javascript">
		$(document).ready(function(){
			$("#personas").jtable({
				title : "Vetplus Registro",
				paging: true, //paginacion de la tabla es verdadera
				pageSize : 4, //nos muestra el numero de registros
				sorting : true, //ordenamiento de registros
				defaultSorting: "date ASC", //manera de ordenamiento
				actions:{
					listAction : "php/acciones.php?action=list",
					createAction: "php/acciones.php?action=create",
					updateAction: "php/acciones.php?action=update",
					deleteAction: "php/acciones.php?action=delete"
				},
				fields:{
					id:{
						key:true,
						create:false,
						edit:false,
						list:false
					},
					user:{
						title:"Veterinario encargado",
						width:"10%"
					},
					date:{
						title:"Fecha",
						width:"10%",
					},
					cliente:{
						title:"Cliente",
						width:"10%"
					},
					correo:{
						title:"Correo",
						width:"10%"
					}

				}
			});

			$("#personas").jtable("load");
		});
	</script>

');
?>