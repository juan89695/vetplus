<?php
include 'db.php';

class User extends DB{
    private $nombre;
    private $username;


    public function userExists($user, $pass){

        $query = $this->connect()->prepare('SELECT * FROM users WHERE username = :user'); 
        $query->execute(['user' => $user]);
        
        if($query->rowCount()){
            $result = $query->fetch();
            if (password_verify($pass, $result['password'])){
                return true;
            }
        }else{
            return false;
        }
    }

    public function createUser($data){

        $query = $this->connect()->prepare('SELECT * FROM users WHERE username = :user');
        $query->execute(['user' => $data[0]]);

        if($query->rowCount()>=1){
            return False;
        }

        $query = $this->connect()->prepare("INSERT INTO users (username,fullname,mail,rol,password) VALUES ( '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]')");
        $query->execute();
        return True;
    }

    public function changePwd($pwd){

        $hash = password_hash($pwd, PASSWORD_DEFAULT);
        $user = $this->username;
        $query = $this->connect()->prepare('UPDATE users SET password = :pass WHERE usuarios.username = :user ');
        $query->execute(['pass'=> $hash, 'user'=> $user]);
    }


    public function setUser($user){
        $query = $this->connect()->prepare('SELECT * FROM users WHERE username = :user');
        $query->execute(['user' => $user]);
        
        foreach ($query as $currentUser) {
            $this->nombre = $currentUser['fullname'];
            $this->username = $currentUser['username'];
        }
    }

    public function countCitas(){
        $query = $this->connect()->prepare('SELECT count(*) as RecordCount from citas');
        $query->execute();
        $result = $query->fetch();
        return $result;
        
    }

    public function getCitas($campoorden, $inicioReg, $finReg){
        $query = $this->connect()->prepare("SELECT * from citas order by :campoorden limit :inicioreg , :finreg");
        $query->execute(['campoorden'=> $campoorden, 'inicioreg'=> $inicioReg, 'finreg'=> $finReg]);
        $result = $query->fetchAll();
        return $result;
        
    }

    public function createCita($user, $cliente, $date, $correo){
        $fp = fopen("miarchivo.txt", "w");
        fwrite($fp, "prueba4");
        $result = $this->connect();
        $query = $result->prepare("INSERT INTO citas (user,date,cliente,correo) VALUES ( '$user', '$date', '$cliente', '$correo')");
        $query->execute();
        $id = $result->lastInsertId();
        $query = $result->prepare("SELECT * FROM citas WHERE id = '$id';");
        $query->execute();
        return  $query->fetch(PDO::FETCH_ASSOC); 
    }

    public function prueba(){
        $result = $this->connect()->lastInsertId();
        return $result;
    }
    public function deleteCita($id){
        $query = $this->connect()->prepare("DELETE FROM citas where id = :id");
        $query->execute(['id'=> $id]);

    }

    public function updateCita($id, $user, $cliente, $date, $correo){
        $fp = fopen("miarchivo.txt", "w");
        fwrite($fp, "prueba3");
        $query = $this->connect()->prepare("UPDATE citas set user='$user', date='$date', cliente='$cliente', correo='$correo' where id='$id'");
        $query->execute();

    }
    

    public function getNombre(){
        return $this->nombre;
    }
    public function getUsername(){
        return $this->username;
    }
}

?>