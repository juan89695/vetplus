<?php 
include_once 'user.php';
$user = new User();

	try {
		if ($_GET['action']=='list') {
			
			$recordCount=$user->countCitas()['RecordCount'];

			//obtenemos datos d paginacion y hacemos el query

			$campoorden=$_GET['jtSorting'];
			$inicioReg=$_GET['jtStartIndex'];
			$finReg=$_GET['jtPageSize'];

			$result=$user->getCitas($campoorden, $inicioReg, $finReg);
			//agregar datos a un arraglo y mandar por json
			$rows=array();
			foreach ($result as $row) {
				$rows[]=$row;
			}

			$jTableResult=array();
			$jTableResult['Result']="OK";
			$jTableResult['TotalRecordCount']=$recordCount;
			$jTableResult['Records']=$rows;

			echo json_encode($jTableResult);
		}else if($_GET['action']=='create'){
			$usuario=$_POST['user'];
			$cliente=$_POST['cliente'];
			$date=$_POST['date'];
			$correo=$_POST['correo'];
			$result = $user->createCita($usuario, $cliente, $date, $correo);
			//$result = $user->createCita("juan9878","sdac","sdcasd","sadsav3523");
			//aqui hacemos el json 
			$jTableResult = array();
			$jTableResult['Result']='OK';
			$jTableResult['Record']=$result;

			echo json_encode($jTableResult);

		}else if($_GET['action']=='update'){
			$id=$_POST['id'];
			$usuario=$_POST['user'];
			$date=$_POST['date'];
			$cliente=$_POST['cliente'];
			$correo=$_POST['correo'];

			$user->updateCita($id, $usuario, $cliente, $date, $correo);

			$jTableResult=array();
			$jTableResult['Result']='OK';
			echo json_encode($jTableResult);

		}else if($_GET['action']=='delete'){
			$id=$_POST['id'];

			$user->deleteCita($id);
			
			$jTableResult=array();
			$jTableResult['Result']='OK';
			echo json_encode($jTableResult);
		}
		
	} catch (Exception $e) {
		$jTableResult=array();
		$jTableResult['Result']="ERROR";
		$jTableResult['Message']="sanjcns";
		echo json_encode($jTableResult);
	}

 ?>